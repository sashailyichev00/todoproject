package main

import (
	"ToDoAuth/internal/app"
	"ToDoAuth/internal/app/domain/auth"
	"ToDoAuth/internal/app/server/jwt"
	"ToDoAuth/internal/app/server/rest"
	"ToDoAuth/internal/app/storage/postgres"
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"
)

func main() {
	cfg, err := app.NewConfig()
	if err != nil {
		panic(fmt.Errorf("failed init app config: %v", err))
	}
	log.Println("init to config...")

	authRep, err := postgres.NewAuthorizationRepository(cfg.DbConfig)
	if err != nil {
		panic(fmt.Errorf("failed init repository: %v", err))
	}
	log.Println("create connection database...")

	tokenService := jwt.InitJwt(cfg.AuthConfig)

	server := rest.NewServer(
		cfg,
		auth.NewService(authRep, tokenService),
		tokenService,
	)
	serverErrChan := make(chan error)
	err = server.Run(serverErrChan)
	if err != nil {
		panic(fmt.Errorf("failed start server: %v", err))
	}
	log.Println("server started")

	quitChan := make(chan os.Signal)
	signal.Notify(quitChan, os.Interrupt, os.Kill)

	select {
	case err = <-serverErrChan:
		fmt.Errorf("error from server: %v", err)
	case <-quitChan:
		log.Println("os interrupt")
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	err = server.Shutdown(ctx)
	if err != nil {
		log.Fatal("cannot shutdown server")
	}

	log.Println("server shutdown")
}
