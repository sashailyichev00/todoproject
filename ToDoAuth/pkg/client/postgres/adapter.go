package postgres

import (
	"ToDoAuth/internal/app"
	"ToDoAuth/pkg/utils"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"time"
)

type Adapter struct {
	Config app.DbConfig

	Client *sql.DB
}

func (a *Adapter) OpenConnection() (func(), error) {
	c := a.Config
	connString := fmt.Sprintf("host=%s port=%s user=%s password=%s database=%s sslmode=disable", c.Host, c.Port, c.Username, c.Password, c.Database)

	client, err := sql.Open("postgres", connString)
	if err != nil {
		log.Fatal(err)
	}
	a.Client = client

	return func() {
		err := a.Client.Close()
		if err != nil {
			log.Fatal(err)
		}
		a.Client = nil
	}, nil
}

func (a *Adapter) Ping() (err error) {
	err = repeatable.DoWithTries(func() error {
		closeConnection, err := a.OpenConnection()
		if err != nil {
			return err
		}
		defer closeConnection()

		return nil
	}, 5, time.Second*5)
	return err
}
