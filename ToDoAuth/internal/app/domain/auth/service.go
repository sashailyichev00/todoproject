package auth

import (
	"context"
	"fmt"
)

type Service struct {
	r repository
	t tokenGenerator
}

func NewService(r repository, t tokenGenerator) *Service {
	return &Service{r: r, t: t}
}

func (s *Service) Register(ctx context.Context, rq User) (string, error) {
	u := User{
		Login:    rq.Login,
		Password: rq.Password,
		Email:    rq.Email,
	}

	userID, err := s.r.InsertUser(ctx, u)
	if err != nil {
		return "", err
	}

	u.ID = userID

	token, err := s.t.GenerateToken(u)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (s *Service) Login(ctx context.Context, rq LoginUser) (string, error) {
	user, err := s.r.Login(ctx, rq.Login)
	if err != nil {
		return "", err
	}
	if user.Password != rq.Password {
		return "", fmt.Errorf("incorrect password")
	}

	t, err := s.t.GenerateToken(user)
	if err != nil {
		return "", err
	}
	return t, nil
}
