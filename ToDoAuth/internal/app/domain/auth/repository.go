package auth

import (
	"context"
)

type repository interface {
	InsertUser(ctx context.Context, user User) (int, error)
	Login(ctx context.Context, login string) (User, error)
}
