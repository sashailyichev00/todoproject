package auth

type tokenGenerator interface {
	GenerateToken(user User) (string, error)
}
