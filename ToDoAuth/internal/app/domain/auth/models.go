package auth

type User struct {
	ID       int
	Login    string
	Email    string
	Password string
}

type LoginUser struct {
	Login    string
	Password string
}
