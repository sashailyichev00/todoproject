package app

import (
	"os"
	"sync"
)

type Config struct {
	Listen     Listen
	DbConfig   DbConfig
	AuthConfig AuthConfig
}

type Listen struct {
	BindIP string
	Port   string
}

type DbConfig struct {
	Host     string
	Port     string
	Username string
	Password string
	Database string
}

type AuthConfig struct {
	JwtSecret          string
	TokenExpirationMin int
}

var instance Config
var once sync.Once

func NewConfig() (Config, error) {
	once.Do(func() {
		instance = Config{
			Listen: Listen{
				BindIP: os.Getenv("AUTH_BIND_IP"),
				Port:   os.Getenv("AUTH_PORT"),
			},
			DbConfig: DbConfig{
				Host:     os.Getenv("AUTH_DB_HOST"),
				Port:     os.Getenv("AUTH_DB_PORT"),
				Username: os.Getenv("AUTH_DB_USERNAME"),
				Password: os.Getenv("AUTH_DB_PASSWORD"),
				Database: os.Getenv("AUTH_DB_DATABASE"),
			},
			AuthConfig: AuthConfig{
				JwtSecret:          os.Getenv("AUTH_JWT_SECRET"),
				TokenExpirationMin: 15,
			},
		}
	})
	return instance, nil
}
