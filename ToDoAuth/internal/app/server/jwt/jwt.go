package jwt

import (
	"ToDoAuth/internal/app"
	"ToDoAuth/internal/app/domain/auth"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"time"
)

type JWT struct {
	secret        string
	expirationMin int64
}

func InitJwt(authCfg app.AuthConfig) *JWT {
	return &JWT{secret: authCfg.JwtSecret, expirationMin: int64(authCfg.TokenExpirationMin)}
}

func (j *JWT) GenerateToken(user auth.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_id": user.ID,
		"exp":     time.Now().Add(time.Minute * time.Duration(j.expirationMin)).Unix(),
	})
	return token.SignedString([]byte(j.secret))
}

func (j *JWT) GetUserIDFromToken(token string) (string, error) {
	t, e := j.parseToken(token)
	if e != nil {
		return "", e
	}

	if !t.Valid {
		return "", fmt.Errorf("token is not valid")
	}

	id := t.Claims.(jwt.MapClaims)["user_id"]

	str := fmt.Sprintf("%v", id)

	return str, nil
}

func (j *JWT) parseToken(token string) (*jwt.Token, error) {
	return jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(j.secret), nil
	})
}
