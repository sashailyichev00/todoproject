package rest

import (
	"ToDoAuth/internal/app"
	"context"
	"fmt"
	"net"
	"net/http"
	"time"
)

const (
	authPrefix = "/auth"
	apiPrefix  = "/api"
)

type tokenParser interface {
	GetUserIDFromToken(token string) (string, error)
}

type Server struct {
	config app.Config

	authService authService
	tokenParser tokenParser

	server *http.Server
}

func NewServer(config app.Config, authService authService, tokenParser tokenParser) *Server {
	return &Server{config: config, authService: authService, tokenParser: tokenParser}
}

func (s *Server) Run(errChan chan<- error) error {
	var listener net.Listener
	var listenErr error

	listener, listenErr = net.Listen("tcp", fmt.Sprintf("%s:%s", s.config.Listen.BindIP, s.config.Listen.Port))
	if listenErr != nil {
		return listenErr
	}

	h := s.initHandler()
	s.server = s.initServer(h)

	go func() {
		errChan <- s.server.Serve(listener)
	}()

	return nil
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.server.Shutdown(ctx)
}

func (s *Server) initHandler() http.Handler {
	r := http.NewServeMux()

	r.HandleFunc(authPrefix+"/register", newAuthHandler(s.authService).Register)
	r.HandleFunc(authPrefix+"/login", newAuthHandler(s.authService).Login)

	r.HandleFunc(authPrefix+"/userIdentity", s.UserIdentity)

	return r
}

func (s *Server) initServer(h http.Handler) *http.Server {
	return &http.Server{
		Handler:      h,
		ReadTimeout:  time.Second * 5,
		WriteTimeout: time.Second * 5,
	}
}
