package rest

import (
	"log"
	"net/http"
	"strings"
)

const (
	authCookie = "token"
	userCtx    = "user_id"
)

func (h *Server) UserIdentity(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie(authCookie)
	if err != nil {
		if err == http.ErrNoCookie {
			w.WriteHeader(http.StatusUnauthorized)
			log.Println("empty auth cookie")
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	cookieParts := strings.Split(cookie.Value, " ")

	if len(cookieParts) == 0 {
		w.WriteHeader(http.StatusUnauthorized)
		log.Println("empty auth token")
		return
	}

	userID, err := h.tokenParser.GetUserIDFromToken(cookieParts[0])
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		log.Println(err)
		return
	}

	w.Header().Set(userCtx, userID)
}
