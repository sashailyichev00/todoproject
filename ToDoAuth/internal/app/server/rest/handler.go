package rest

import (
	"ToDoAuth/internal/app/domain/auth"
	"context"
	"encoding/json"
	"log"
	"net/http"
)

type authService interface {
	Register(ctx context.Context, rq auth.User) (string, error)
	Login(ctx context.Context, rq auth.LoginUser) (string, error)
}

type authHandler struct {
	as authService
}

func newAuthHandler(as authService) authHandler {
	return authHandler{as: as}
}

func (ah authHandler) Register(w http.ResponseWriter, r *http.Request) {
	type RegisterRequest struct {
		Login    string `json:"login"`
		Password string `json:"password"`
		Email    string `json:"email"`
	}
	req := RegisterRequest{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&req); err != nil {
		log.Printf("failed to register: %v", err)
		return
	}

	regReq := auth.User{Login: req.Login, Password: req.Password, Email: req.Email}

	token, err := ah.as.Register(r.Context(), regReq)
	if err != nil {
		log.Printf("failed auth: %v", err)
		return
	}

	type RegisterResponse struct {
		Token string `json:"token"`
	}
	resp := RegisterResponse{Token: token}

	w.Header().Add("Content-type", "application/json")
	http.SetCookie(w, &http.Cookie{
		Name:  "token",
		Value: token,
		Path:  "/",
	})
	w.WriteHeader(201)

	log.Println("User registered")

	if err = json.NewEncoder(w).Encode(resp); err != nil {
		return
	}
}

func (ah authHandler) Login(w http.ResponseWriter, r *http.Request) {
	type LoginRequest struct {
		Login    string `json:"login"`
		Password string `json:"password"`
	}
	req := LoginRequest{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&req); err != nil {
		log.Printf("failed login: %v", err)
		return
	}

	loginReq := auth.LoginUser{Login: req.Login, Password: req.Password}

	token, err := ah.as.Login(r.Context(), loginReq)
	if err != nil {
		log.Printf("failed auth: %v", err)
		return
	}

	type LoginResponse struct {
		Token string `json:"token"`
	}
	resp := LoginResponse{Token: token}

	w.Header().Add("Content-type", "application/json")
	http.SetCookie(w, &http.Cookie{
		Name:  "token",
		Value: token,
		Path:  "/",
	})
	w.WriteHeader(201)

	log.Println("User logged in")

	if err = json.NewEncoder(w).Encode(resp); err != nil {
		return
	}
}
