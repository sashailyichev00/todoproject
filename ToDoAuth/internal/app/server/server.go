package server

import "context"

type Server interface {
	Run(errChan chan<- error) error
	Shutdown(ctx context.Context) error
}
