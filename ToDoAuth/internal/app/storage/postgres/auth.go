package postgres

import (
	"ToDoAuth/internal/app"
	"ToDoAuth/internal/app/domain/auth"
	"ToDoAuth/pkg/client/postgres"
	"context"
)

type AuthRepository struct {
	dbConfig app.DbConfig
}

func NewAuthorizationRepository(cfg app.DbConfig) (AuthRepository, error) {
	plAdapter := postgres.Adapter{Config: cfg}
	if err := plAdapter.Ping(); err != nil {
		return AuthRepository{}, err
	}
	return AuthRepository{dbConfig: cfg}, nil

}

func (a AuthRepository) InsertUser(ctx context.Context, u auth.User) (int, error) {
	plAdapter := postgres.Adapter{Config: a.dbConfig}
	closeConnection, err := plAdapter.OpenConnection()
	if err != nil {
		return -1, err
	}
	defer closeConnection()

	q := "insert into Users(Username, Password, Email) values ($1, $2, $3) returning id"

	if err := plAdapter.Client.QueryRow(q, u.Login, u.Password, u.Email).Scan(&u.ID); err != nil {
		return -1, err
	}

	return u.ID, nil
}

func (a AuthRepository) Login(ctx context.Context, login string) (auth.User, error) {
	plAdapter := postgres.Adapter{Config: a.dbConfig}
	closeConnection, err := plAdapter.OpenConnection()
	if err != nil {
		return auth.User{}, err
	}
	defer closeConnection()

	u := auth.User{}

	q := "select ID, Username, Password from Users where Username = $1"
	if err := plAdapter.Client.QueryRow(q, login).Scan(&u.ID, &u.Login, &u.Password); err != nil {
		return auth.User{}, err
	}

	return u, nil
}
