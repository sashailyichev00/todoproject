package postgres

type Users struct {
	ID       int
	Login    string
	Password string
	Email    string
}
