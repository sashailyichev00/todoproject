# TO-DO-SERVER

## Database

Used data store `Postgresql` and `Elasticsearch`. Postgresql integrated [migrations](./ToDoAuth/migrations) in deploy process.

## Authentication

Auth used JWT tokens

## Configuration

Server configures enviroment variables:

- auth_api:
    - AUTH_BIND_IP - Web-service listening address
    - AUTH_PORT - Web-service port
    - AUTH_DB_HOST - Postgresql host
    - AUTH_DB_PORT - Postgresql port
    - AUTH_DB_USERNAME - Postgresql username
    - AUTH_DB_PASSWORD - Postgresql password
    - AUTH_DB_DATABASE - Postgresql database name
    - AUTH_JWT_SECRET - JWT token secret

- auth_pg_db:
    - POSTGRES_DB - Create postgresql database name
    - POSTGRES_PASSWORD - Postgresql password

- todo_api:
    - LIST_TYPE - Web-service listening type
    - LIST_BIND_IP - Web-service listening address
    - LIST_PORT - Wer-service port
    - LIST_URL_1 - Elasticsearch connection url

- todo_elastic:
    - "discovery.type=single-node" - Elastic running single node

## Deploy

Docker-compose file [example](./docker-compose.yml)

Makefile file [example](./Makefile):
    - dev-run (default) - Building and running docker images
    - dev-stop - Stoping docker-compose