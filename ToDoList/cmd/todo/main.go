package main

import (
	"ToDoList/internal/app"
	"ToDoList/internal/app/server"
	"ToDoList/internal/app/todo/elastic"
	"ToDoList/pkg/client/elasticsearch"
	"fmt"
)

func main() {
	cfg, err := app.NewConfig()
	if err != nil {
		panic(fmt.Errorf("failed init config: %s", err))
	}

	es, err := elasticsearch.NewElasticClient(cfg.DbConfig)
	if err != nil {
		panic(fmt.Errorf("failed init elastic: %s", err))
	}

	newToDoRepository := elastic.NewToDoRepository(es.Client)

	server.Start(
		cfg,
		newToDoRepository,
	)
}
