package elasticsearch

import (
	"ToDoList/internal/app"
	"github.com/elastic/go-elasticsearch"
)

type Adapter struct {
	Config app.DbConfig

	Client *elasticsearch.Client
}

func NewElasticClient(cfg app.DbConfig) (Adapter, error) {
	es, err := elasticsearch.NewClient(elasticsearch.Config{
		Addresses: cfg.URL,
	})
	if err != nil {
		return Adapter{}, err
	}

	return Adapter{Config: cfg, Client: es}, nil
}
