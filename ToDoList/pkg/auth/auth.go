package auth

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

const (
	userCtx = "user_id"
)

func GetUserId(w http.ResponseWriter, r *http.Request) int {
	w.Header().Add("Content-type", "application/json")
	ui := w.Header().Get("Set-Cookie")
	if ui == "" {
		id, err := r.Cookie(userCtx)
		if err != nil {
			if err == http.ErrNoCookie {
				w.WriteHeader(http.StatusUnauthorized)
				fmt.Println("auth error")
				return -1
			}
			w.WriteHeader(http.StatusBadRequest)
			fmt.Println("auth error")
			return -1
		}

		idInt, err := strconv.Atoi(id.Value)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			fmt.Println(err)
			return -1
		}
		return idInt
	}

	mapUi := strings.Split(ui, ";")

	uiInt, err := strconv.Atoi(strings.ReplaceAll(mapUi[0], "user_id=", ""))
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Println(err)
		return -1
	}

	return uiInt
}
