package hanlers

import (
	"log"
	"net/http"
)

const (
	userCtx = "user_id"
	urlAuth = "http://auth_api:8080/auth/userIdentity"
)

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := r.Cookie("user_id")

		if err != nil {
			c, err := r.Cookie("token")
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				log.Println(err)
				return
			}

			cl := &http.Client{}

			cookie := &http.Cookie{
				Name:  c.Name,
				Value: c.Value,
				Path:  c.Path,
			}

			req, err := http.NewRequest("GET", urlAuth, nil)
			if err != nil {
				log.Println(err)
				return
			}

			req.AddCookie(cookie)

			resp, err := cl.Do(req)
			if err != nil {
				log.Println(err)
				return
			}

			defer resp.Body.Close()

			http.SetCookie(w, &http.Cookie{
				Name:  "user_id",
				Value: resp.Header.Get("User_id"),
				Path:  "/",
			})
		}

		next.ServeHTTP(w, r)
	})
}
