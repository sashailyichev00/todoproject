package app

import (
	"os"
	"sync"
)

type Config struct {
	Listen   Listen
	DbConfig DbConfig
}

type Listen struct {
	Type   string
	BindIP string
	Port   string
}

type DbConfig struct {
	URL []string
}

var instance Config
var once sync.Once

func NewConfig() (Config, error) {
	once.Do(func() {
		instance = Config{
			Listen: Listen{
				Type:   os.Getenv("LIST_TYPE"),
				BindIP: os.Getenv("LIST_BIND_IP"),
				Port:   os.Getenv("LIST_PORT"),
			},
			DbConfig: DbConfig{
				URL: []string{
					os.Getenv("LIST_URL_1"),
				},
			},
		}
	})
	return instance, nil
}
