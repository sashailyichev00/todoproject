package todo

import (
	"ToDoList/internal/app/hanlers"
	"ToDoList/pkg/auth"
	"context"
	"encoding/json"
	"log"
	"net/http"
)

const (
	toDoPrefix      = "/api/toDo"
	toDoListsPrefix = "/lists"
)

type handler struct {
	repository Repository
}

func NewHandler(repository Repository) hanlers.Handler {
	return &handler{repository: repository}
}

func (h *handler) Register(router *http.ServeMux) {
	router.HandleFunc(toDoPrefix+toDoListsPrefix, h.Lists)
	router.HandleFunc(toDoPrefix+"/listUpdate", h.UpdateList)
}

//func (h *handler) Create()

func (h *handler) Lists(w http.ResponseWriter, r *http.Request) {
	u := auth.GetUserId(w, r)
	ctx := context.Background()
	list := List{}
	switch r.Method {
	case http.MethodGet:
		l := h.repository.GetAllLists(ctx, u)
		allByte, err := json.Marshal(l)
		if err != nil {
			w.WriteHeader(400)
			log.Printf("error converting to json: %v", err)
		}
		w.Write(allByte)
	case http.MethodPost:
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&list); err != nil {
			log.Printf("failed decode json: %v", err)
			return
		}
		h.repository.CreateList(ctx, u, list)
		w.Write([]byte("index created"))
	case http.MethodDelete:
		lr := ListResponse{}
		decoder := json.NewDecoder(r.Body)
		if err := decoder.Decode(&lr); err != nil {
			log.Printf("failed decode json: %v", err)
			return
		}
		h.repository.DeleteList(ctx, lr.IndexId)
		w.Write([]byte("index deleted"))
	default:
		w.Write([]byte("another method"))
	}
}

func (h *handler) UpdateList(w http.ResponseWriter, r *http.Request) {
	//u := auth.GetUserId(w, r)
	ctx := context.Background()
	lr := ListResponse{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&lr); err != nil {
		log.Printf("failed decode json: %v", err)
		return
	}
	h.repository.UpdateList(ctx, lr)
	allByte, err := json.Marshal(lr)
	if err != nil {
		w.WriteHeader(400)
		log.Printf("error converting to json: %v", err)
	}
	w.Write(allByte)
}
