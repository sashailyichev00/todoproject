package todo

type List struct {
	Name   string `json:"name"`
	Done   bool   `json:"done"`
	UserId int    `json:"user_id"`
}

type ListResponse struct {
	IndexId string `json:"_id"`
	List    List
}
