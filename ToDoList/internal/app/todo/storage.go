package todo

import "context"

type Repository interface {
	CreateList(ctx context.Context, userID int, list List)
	UpdateList(ctx context.Context, list ListResponse)
	DeleteList(ctx context.Context, listID string)
	GetAllLists(ctx context.Context, userID int) []ListResponse
}
