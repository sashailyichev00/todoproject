package elastic

import (
	"ToDoList/internal/app/todo"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/elastic/go-elasticsearch"
	"github.com/elastic/go-elasticsearch/esapi"
	"log"
	"strconv"
	"strings"
)

type repository struct {
	client *elasticsearch.Client
}

const (
	indexName = "lists"
)

func NewToDoRepository(client *elasticsearch.Client) todo.Repository {
	return &repository{
		client: client,
	}
}

func (r *repository) CreateList(ctx context.Context, userID int, list todo.List) {
	l := todo.List{
		Name:   list.Name,
		Done:   list.Done,
		UserId: userID,
	}
	dataJson, err := json.Marshal(l)
	if err != nil {
		log.Printf("failed json: %v", err)
		return
	}
	js := string(dataJson)

	req := esapi.IndexRequest{
		Index:   indexName,
		Body:    strings.NewReader(js),
		Refresh: "true",
	}

	e, err := req.Do(ctx, r.client)
	if err != nil {
		log.Printf("failed create: %v", err)
		return
	}

	defer e.Body.Close()
	log.Println("Index created")
}

func (r *repository) UpdateList(ctx context.Context, list todo.ListResponse) {
	query := `{"doc": {"name": "` + list.List.Name + `", "done": ` + strconv.FormatBool(list.List.Done) + `}}`
	res, err := r.client.Update(
		indexName,
		list.IndexId,
		strings.NewReader(query),
		r.client.Update.WithContext(ctx),
		r.client.Update.WithPretty(),
	)
	if err != nil {
		log.Printf("failed update index: %v", err)
		return
	}
	fmt.Println(res)
	return
}

func (r *repository) DeleteList(ctx context.Context, listID string) {
	res, err := r.client.Delete(
		indexName,
		listID,
		r.client.Delete.WithContext(ctx),
		r.client.Delete.WithPretty(),
	)
	if err != nil {
		log.Printf("failed delete index: %v", err)
		return
	}
	fmt.Println(res)
	return
}

func (r *repository) GetAllLists(ctx context.Context, userID int) (l []todo.ListResponse) {
	var mapResp map[string]interface{}

	strUserID := strconv.Itoa(userID)

	var buf bytes.Buffer
	var b strings.Builder

	query := `{"query": {"match": {"user_id": "` + strUserID + `"}}}`
	b.WriteString(query)
	read := strings.NewReader(b.String())

	if err := json.NewEncoder(&buf).Encode(read); err != nil {
		log.Printf("error encoding query: %v", err)
	}

	res, err := r.client.Search(
		r.client.Search.WithContext(ctx),
		r.client.Search.WithIndex(indexName),
		r.client.Search.WithBody(read),
		r.client.Search.WithTrackTotalHits(true),
		r.client.Search.WithPretty(),
	)
	if err != nil {
		log.Printf("failed query: %v", err)
		return nil
	}

	defer res.Body.Close()

	if err := json.NewDecoder(res.Body).Decode(&mapResp); err != nil {
		log.Printf("Error parsing the response body: %v", err)
		return nil
	}

	lists := make([]todo.ListResponse, 0)

	for _, hit := range mapResp["hits"].(map[string]interface{})["hits"].([]interface{}) {
		var ls todo.ListResponse
		doc := hit.(map[string]interface{})
		source := doc["_source"].(map[string]interface{})
		docID := doc["_id"].(string)

		js, err := json.Marshal(source)
		if err != nil {
			log.Printf("failed convert to json: %v", err)
			return nil
		}

		ls.IndexId = docID
		if err := json.Unmarshal(js, &ls.List); err != nil {
			log.Printf("failed convert to struct: %v", err)
			return nil
		}

		lists = append(lists, ls)
	}

	return lists
}
