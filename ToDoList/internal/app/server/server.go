package server

import (
	"ToDoList/internal/app"
	"ToDoList/internal/app/hanlers"
	"ToDoList/internal/app/todo"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"time"
)

func Start(cfg app.Config, toDo todo.Repository) {
	var listener net.Listener
	var listenErr error

	if cfg.Listen.Type == "sock" {
		fmt.Println("detect app path")
		appDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("create socket")
		socketPath := path.Join(appDir, "app.sock")

		fmt.Println("listen unix socket")
		listener, listenErr = net.Listen("unix", socketPath)
		fmt.Printf("server is listening unix socket: %s", socketPath)
	} else {
		fmt.Println("listen tcp")
		listener, listenErr = net.Listen("tcp", fmt.Sprintf("%s:%s", cfg.Listen.BindIP, cfg.Listen.Port))
		fmt.Printf("server is listening port %s:%s\n", cfg.Listen.BindIP, cfg.Listen.Port)
	}

	if listenErr != nil {
		log.Fatal(listenErr)
	}

	router := initHandler(toDo)

	server := &http.Server{
		Handler:      router,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(server.Serve(listener))
}

func initHandler(toDo todo.Repository) http.Handler {
	r := http.NewServeMux()

	toDoHandler := todo.NewHandler(toDo)
	toDoHandler.Register(r)

	handler := hanlers.AuthMiddleware(r)

	return handler
}
