.PHONY: build
dev-run:
	docker-compose -f ./docker-compose.yml up -d
dev-stop:
	docker-compose -f ./docker-compose.yml down

.DEFAULT_GOAL := dev-run
